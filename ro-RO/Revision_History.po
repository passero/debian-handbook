#
# AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr "Project-Id-Version: 0\nPOT-Creation-Date: 2015-09-09 18:47+0200\nPO-Revision-Date: 2022-10-10 05:47+0000\nLast-Translator: Florin Voicu <florin.bkk@gmail.com>\nLanguage-Team: Romanian <https://hosted.weblate.org/projects/debian-handbook/revision_history/ro/>\nLanguage: ro-RO\nMIME-Version: 1.0\nContent-Type: application/x-publican; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;\nX-Generator: Weblate 4.14.1\n"

msgid "Revision History"
msgstr "Istoric modificări"

msgid "Raphaël"
msgstr "Raphaël"

msgid "Hertzog"
msgstr "Hertzog"
